Source: qiskit-ibmq-provider
Section: science
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Diego M. Rodriguez <diego@moreda.io>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-arrow,
               python3-nest-asyncio,
               python3-pytest,
               python3-qiskit-terra (>= 0.12.0-3~),
               python3-setuptools,
               python3-requests,
               python3-requests-ntlm,
               python3-websockets,
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/science-team/qiskit-ibmq-provider
Vcs-Git: https://salsa.debian.org/science-team/qiskit-ibmq-provider.git
Homepage: https://qiskit.org
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-qiskit-ibmq-provider
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends},
         python3-arrow,
         python3-nest-asyncio,
         python3-qiskit-terra (>= 0.12.0-3~),
         python3-requests,
         python3-requests-ntlm,
         python3-websockets,
Description: Quantum Information Science Kit (Qiskit): IBM Q Provider
 Qiskit (Quantum Information Science Kit) is a collection of software developed
 by IBM Research for working with short depth quantum circuits and building
 near term applications and experiments on quantum computers.
 .
 This package contains a provider that allows accessing the online IBM Q
 quantum devices and simulators.
